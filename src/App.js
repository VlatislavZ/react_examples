import React from 'react';
import logo from './logo.svg';
import './App.css';
import ContentBlock from "./components/ContentBlock";
import FooterBlock from "./components/FooterBlock";
import HeaderBlock from "./components/HeaderBlock";
import wordsList from './wordsList'
import Title from "./components/Title";
import Paragraph from "./components/Paragraph";
import CardList from "./components/CardList";
import Loader from "./components/Loader";
import Effects from './components/Effects'
function App() {
    return (<>
            <HeaderBlock LogoImage={logo} animate LogoLabel="React"/>
            <ContentBlock showImage>
                <Effects/>
            <Loader/>
                <Title>Учите слова онлайн</Title>
                <Paragraph>Воспользуйтесь карточками для запоминания и пополнения активных словарных
                    запасов</Paragraph>
            </ContentBlock>
            <ContentBlock>
                <Title>Поехали!</Title>
                <CardList items={wordsList}/>
            </ContentBlock>
            <FooterBlock year={2020}/>
        </>
    );
}

export default App;





// import React from 'react';
// import HeaderBlock from "./components/HeaderBlock";
// import Header from "./components/Title";
// import Paragraph from "./components/Paragraph";
// import MainBlock from "./components/ContentBlock"
// import CardList from "./components/CardList";
// // import Card from "./components/Card";
// import {wordsList} from './wordsList'
// // import {ReactComponent as ReactLogo} from "./logo.svg";
// const App  = () => {
//     return (
//         <>
//             <HeaderBlock hideBackground="true">
//                 <Header>Привет, я тут занимаюсь изучением фреймворка</Header>
//                 <h2>REACT</h2>
//                 <Paragraph>Это параграф, как отдельный элемент</Paragraph>
//                 <em>а это не компонент реакта, просто текст в теге EM </em>
//             </HeaderBlock>
//             {/*<div>*/}
//             {/*    <Paragraph>Тут список элементов, пропсами работаю, тут пытался сделать из функционального в компонеты-классы</Paragraph>*/}
//             {/*    {*/}
//             {/*        //лучше использовать уникальный id когда мы берешь массив аргументов,*/}
//             {/*        //ведь не обязательно же вы будем класть и удалять элементы в конце(с конца)*/}
//             {/*        //тогда переопределение будет работать не на нас!(*/}
//             {/*        wordsList*/}
//             {/*            .map(({eng,rus},index) =>*/}
//             {/*                <Card key = {index} eng={eng} rus={rus}/>)*/}
//             {/*    }*/}
//             {/*</div>*/}
//             <Paragraph>Тут список элементов, пропсами работаю, тут пытался сделать из функционального в компонеты-классы</Paragraph>
//             <CardList items={wordsList}/>
//
//             <HeaderBlock>
//                 <Header>Тут обычный блок в котором заголовок(вот он я)</Header>
//                 <Paragraph>и параграф! (вот это я параграф)</Paragraph>
//             </HeaderBlock>
//         <MainBlock><h1>Тут я создал другой реакт элемент и на background поставил картинку</h1></MainBlock>
//         </>
//     );
// }
//
// export default App;
