import React from 'react'
import s from './Effets.module.css'

const Effects = ({children}) => {
    return(
        <div className={s.container}>
            <span className={s.text1}>Let's PLAY</span>
            <span className={s.text2}>Press ENTER =)</span>
        </div>
    )
}
export default Effects;