import React from "react";
import './style.css'

const ContentBlock = ({showImage = false, children}) => {
    return (
        <div className="cover">
            {showImage ? <div className={"ImageBg"}/> : null}
            <div className="wrap">
                {children}
            </div>
        </div>
    );
};

export default ContentBlock;