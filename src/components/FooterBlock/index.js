import React from "react";
import './FooterBlock.css'
// import cl from 'classnames'

const FooterBlock = ({year}) => {
    return (
        <div className={"Copyright"}>
            <a href="https://github.com/Vlatislav">

            </a>
        <span className={"CopyrightText"}>
            <span className={"SecondaryText"}>Made by</span>
            {' '}
            <a href="https://github.com/Vlatislav"
               rel="noopener noreferrer"
               target="_blank" className={"mygit"}>Uladzislau Litvinau</a>
            {' '}
            <span className={"SecondaryText"}>| {year}</span>
        </span>
        </div>
    )
}
export default FooterBlock