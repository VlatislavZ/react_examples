import React from "react";
// import {LikeOutlined} from "@ant-design/icons";
import s from './HeaderBlock.module.css'

const HeaderBlock = ({LogoImage, LogoLabel, animate = false}) => {
    return <div>
        <nav className={s.Navigation}>
            <img className={animate ? s.LogoImageA : null } src={LogoImage} alt={LogoLabel}/>
            <label className={s.Label}>
                <span className={s.React}>React</span>
                <span className={s.Marathon}>marathon</span>
                {/*<LikeOutlined className={s.Like}/>*/}
            </label>
        </nav>
    </div>
}

export default HeaderBlock




// import React from "react";
// import s from './HeaderBlock.module.css'
// //можно props а можно объект {title}
// const HeaderBlock = ({hideBackground = false, children}) => {
//     //console.log('sad',props)//видно что свойство children нам и нужно!
//     //меня. ...props на children
//     const back = hideBackground ? {backgroundColor: '#ae7676', fontSize:'20px'} : null;
//     return(
//         <div className={s.cover} style={back}>
//             {/*<h1 className={s.header}>{props.title}</h1>*/}
//             {/*{title && <h1 className={s.header}>{title}</h1>}*/}
//             {/*<p>Что-то написать нужно было для примера</p>*/}
//
//             {children}
//         </div>
//     )
// }
// export default HeaderBlock;