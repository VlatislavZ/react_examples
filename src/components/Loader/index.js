import React from 'react'
import s from './Loader.module.css'

const Loader = ({children}) => {
    return(
        <div className={s.loader + ' inverted'}>{children}</div>
    )
}
export default Loader;