import React,{Component} from 'react';
import s from './Card.module.scss';
import cl from 'classnames'

class Card extends Component {
    state = {
        flipped: false,
    }

    handleClick = () => {
        this.setState(({flipped}) => {
            return {flipped: !flipped}
        });
    }

    render() {
        const {eng, rus} = this.props;
        const {flipped} = this.state;
        return (
            <div onClick={this.handleClick} className={cl(s.card, {[s.flip]: flipped})}>
                <div className={s.cardInner}>
                    <div  onClick={s.onclick} className={s.cardFront+ ' inverted'}>
                        {eng}
                    </div>
                    <div  className={s.cardBack+ ' inverted'}>
                        {rus}
                    </div>
                </div>
            </div>
        );
    }
}

// class Card extends React.Component{
//
//     state = {
//         done : false,
//     }
//
//     //функция обработки нажатия на карточку, убрал эффект переворачивания)
//     onClickCard = () => {
//         if(!this.state.done){this.setState({done : true,})}
//         else                {this.setState({done : false,})}
//     }
//
//     render(){
//
//         const {eng,rus} = this.props;
//         //нажали или нет
//         const {done} = this.state;
//
//         // const cardClass = [s.card]
//         //
//         // if(done){
//         //     cardClass.push(s.done)
//         // }
//
//         return(
//             <div
//                  // className={cardClass.join(' ')}
//                 className={cn(s.card,{[s.done] : done})}
//                  onClick={this.onClickCard}
//                  onCopy={()=> console.log(`DON'T COPY CARD NAME!!!Error('COPY CARD NAME')`)}
//             >
//                 <div className={s.cardInner}>
//                     <div className={s.cardFront}>
//                         { eng }
//                     </div>
//                     <div className={s.cardBack}>
//                         { rus }
//                     </div>
//                 </div>
//             </div>
//         )
//     }
// }

// const Card = ({eng, rus}) => {
//     return (
//             <div className={s.card}>
//                 <div className={s.cardInner}>
//                     <div className={s.cardFront}>
//                         { eng }
//                     </div>
//                     <div className={s.cardBack}>
//                         { rus }
//                     </div>
//                 </div>
//             </div>
//         );
// }

export default Card;