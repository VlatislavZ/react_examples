import React from "react";
import s from './Title.module.css'

const Title = ({children}) => {
    return <h1 className={s.header}>
        {children}
    </h1>
}
export default Title


// import React from "react";
// import s from './Title.module.css'
//
// const Header = ({children}) =>{
//     return <h1 className={s.header}>{children}</h1>
// };
// export default Header;